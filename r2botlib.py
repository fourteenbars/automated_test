import time
import os
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from random import randint
from selenium.common.exceptions import TimeoutException


class botlib():

        def __init__(self):
            # Get the browser instance
            self.driver = webdriver.Firefox()


        def get_bot_type(self):
            # get bot name using class name of element
            bot_name = self.driver.find_element_by_class_name('_3oh-')
            return bot_name.text


        # @staticmethod
        def get_page(self,url, title_):
            # Takes url and title of site
            self.driver.get(url)
            # verify the title of the page
            assert title_ in self.driver.title

        def login(self,email,password_):
            # Get username field
            username = self.driver.find_element_by_xpath("//*[@id='email']")
            # Send
            username.send_keys(email)

            password = self.driver.find_element_by_xpath("//*[@id='pass']")
            password.send_keys(password_)

            button = self.driver.find_element_by_xpath("//*[@id='loginbutton']")
            button.click()
            WebDriverWait(self.driver,60).until(EC.presence_of_all_elements_located((By.CSS_SELECTOR, "div._5f0v")))

        def send_text(self,message):
            text_area = self.driver.find_element_by_css_selector("div._1mf._1mj")
            text_area.click()
            self.driver.switch_to_active_element().send_keys(message)
            self.driver.switch_to_active_element().send_keys(Keys.RETURN)


        def click_area(self,method, value):
            click_element= ''
            if(method == 'XPATH'):
                WebDriverWait(self.driver,60).until(EC.presence_of_all_elements_located((By.XPATH, value)))
                click_element = self.driver.find_element_by_xpath(value)
                click_element.click()
            elif(method == 'CSS_SELECTOR'):
                WebDriverWait(self.driver,60).until(EC.presence_of_all_elements_located((By.CSS_SELECTOR, value)))
                click_element = self.driver.find_element_by_css_selector(value)
                click_element.click()



        def log_into_file(self,message):

            try:
                msg = message
                localtime = time.asctime( time.localtime(time.time()) )
                # check if the file exists
                filename = "bot.log"
                if(os.path.isfile(filename)):
                    myfile = open("bot.log", "a")

                else:
                    myfile = open("bot.log", "w+")

                myfile.write( "\n %s : \t %s" % (localtime, msg))

            except IOError:
                print "Error trying to create  and read into file"
            except Exception as ex:
                print ex



        def randomize_to_click(self,element_):

                try:

                    WebDriverWait(self.driver,6).until(EC.presence_of_all_elements_located((By.XPATH, " (//div[@class = '_3bwx'])")))
                    new_toggle_num = len(self.driver.find_elements_by_xpath(" (//div[@class = '_3bwx'])"))
                except TimeoutException as Ex:
                    new_toggle_num = 0

                try: toggle_num
                except NameError:
                    toggle_num = 0
                # randomize
                rand = randint((cnr+1), n_cnr)
                if((new_toggle_num-toggle_num)>0):
                    # check if value of rand is accessible without toggling
                    if(rand <=(cnr + 2)):
                        get_answer = self.driver.find_element_by_xpath( element_ + "[%d]"% rand)
                        get_answer.click()
                        try:
                            WebDriverWait(self.driver,5).until(EC.presence_of_all_elements_located((By.XPATH, " (//div[@class = '_3bwx'])" + "[%d]"% (new_toggle_num+1))))
                            # take a screenshot
                            self.screen_shot_to_folder()
                        except TimeoutException as ex:
                            error = "This probably means there are no new cards"
                            # do a yes or no test
                            try:
                                ans = self.select_quick_reply()
                            except Exception as Ex:
                                err = Ex
                        else:
                            new_toggle_num = len(self.driver.find_elements_by_xpath(" (//div[@class = '_3bwx'])"))



                    else:
                        x=1
                        toggle_right = self.driver.find_element_by_xpath(" (//div[@class = '_3bwx'])[%d]"%new_toggle_num)
                        # return(new_toggle_num, toggle_num, cnr, n_cnr)
                        while (x <= (rand-(cnr+2))):


                            toggle_right.click()
                            x= x+1
                        get_answer = self.driver.find_element_by_xpath( element_ + "[%d]"% rand)

                        get_answer.click()

                        try:
                            WebDriverWait(self.driver,5).until(EC.presence_of_all_elements_located((By.XPATH, " (//div[@class = '_3bwx'])" + "[%d]"% (new_toggle_num+1))))
                            # take a screenshot
                            self.screen_shot_to_folder()
                        except TimeoutException as ex:
                            error = "This probably means there are no new cards"
                            # do a yes or no test
                            try:
                                ans = self.select_quick_reply()
                            except Exception as Ex:
                                err = Ex

                        else:
                            n_tog_num = len(self.driver.find_elements_by_xpath(" (//div[@class = '_3bwx'])"))
                            toggle_num = new_toggle_num
                            new_toggle_num = n_tog_num
                        return(new_toggle_num, toggle_num, cnr, n_cnr)

                else:
                    get_answer = self.driver.find_element_by_xpath( element_ + "[%d]"% rand)
                    get_answer.click()
                    self.screen_shot_to_folder()



        def random_select(self,element_):
                global cnr,n_cnr
                WebDriverWait(self.driver,60).until(EC.presence_of_all_elements_located((By.XPATH, element_)))
                n_cnr =  len(self.driver.find_elements_by_xpath(element_))
                try: cnr
                except NameError:
                    cnr = 0
                if((n_cnr-cnr)>0):

                    while((n_cnr-cnr)>0):

                        val = self.randomize_to_click(element_)
                        # return val

                        try:
                            WebDriverWait(self.driver,60).until(EC.presence_of_all_elements_located((By.XPATH, element_ +"[%d]"%(n_cnr+1))))
                        except TimeoutException as ex:
                            error = ex
                        finally:
                            new =  len(self.driver.find_elements_by_xpath(element_))
                            cnr = n_cnr
                            n_cnr = new

                    return(val, n_cnr, cnr,ex)

                else:
                    return("yah man")





        def select_quick_reply(self):
            try:
                WebDriverWait(self.driver,6).until(EC.presence_of_all_elements_located((By.XPATH, ("(//div[@class = '_10-e'])"))))
                y_n_num = len(self.driver.find_elements_by_xpath("(//div[@class = '_10-e'])"))
                rand = randint(1, y_n_num)
            except TimeoutException as Ex:
                rand = 0
            finally:
                # WebDriverWait(self.driver,60).until(EC.presence_of_all_elements_located((By.XPATH, ("(//div[@class = '_10-e'])"))))
                while(rand > 0):
                    click_ = self.driver.find_element_by_xpath("(//div[@class = '_10-e'])[%d]"%rand)
                    click_.click()
                    try:
                        WebDriverWait(self.driver,6).until(EC.presence_of_all_elements_located((By.XPATH, ("(//div[@class = '_10-e'])"))))
                        y_n_num = len(self.driver.find_elements_by_xpath("(//div[@class = '_10-e'])"))
                        rand = randint(1, y_n_num)
                    except TimeoutException as Ex:
                        rand = 0
                    if(rand==0):
                        break



        def delete_conversation(self):
            ele = self.driver.find_element_by_css_selector("div._5blh._4-0h")
            hov = ActionChains(self.driver).move_to_element(ele)
            hov.perform()
            ele.click()
            delete = self.driver.find_element_by_css_selector("span._54nh")
            delete = self.driver.find_element_by_xpath("(//span[@class= '_54nh'])[3]")
            delete.click()
            butt = self.driver.find_element_by_xpath("(//button[@class = '_3quh _30yy _2t_ _3ay_ _5ixy'])[1]")
            butt.click()
            self.driver.close()


        def screen_shot_to_folder(self):

            try: y
            except NameError: y = 1
            newpath = 'Screenshots'
            if not os.path.exists(newpath):
                os.makedirs(newpath)

            try:

                assert "MTN" in self.get_bot_type()
                path = r'Screenshots/MTN_CEP_BOT_SCREENSHOTS'
                if not os.path.exists(path):
                    os.makedirs(path)

                while(os.path.exists("%s/Bot_test_%s.png"%(path, y))):
                    y = y+1

                self.driver.save_screenshot("%s/Bot_test_%s.png"%(path, y))


            except AssertionError:
                try:
                    assert "Pizza" in self.get_bot_type()
                    path = r'Screenshots/PAPAS_PIZZA_BOT_SCREENSHOTS'
                    if not os.path.exists(path):
                        os.makedirs(path)
                    while(os.path.exists("%s/Bot_test_%s.png"%(path, y))):
                        y = y+1
                    self.driver.save_screenshot("%s/Bot_test_%s.png"%(path, y))
                except AssertionError:
                    print "Error"


            y = y+1
