import redis
import pymongo
import elasticsearch
from elasticsearch import Elasticsearch
import mysql.connector
from mysql.connector import errorcode

class db_connection_test():
  # function for testing mongo_db connection
    def mongo_db(self):
        try:
            mongo_exception_msg = ""
            conn = pymongo.MongoClient('localhost', 27017)
            collectionNames = conn["testdb"].collection_names()
            if collectionNames:
                mongo_db_status = "active"
            elif not len(collectionNames):
                mongo_db_status = "Database is empty or does not exist"
            else: mongo_db_status = "inactive"

            # print "Collection Names", collectionNames
            conn.close()
        except Exception as ex:
            # mongo_exception_msg = ""
            mongo_exception_msg = "Error: " +  ex
            # exit('Failed to connect, terminating.')
        finally:
            return(mongo_db_status, mongo_exception_msg)

#function for testing redis db Connection
    def redis_db(self):
            try:
                redis_exception_msg = ""
                redis_db_status = ""
                conn = redis.StrictRedis(
                    host='localhost',
                    port=6379,
                    password='')
                # print conn
                # conn.ping()
                # print 'Connected!'
                #try:
                set_record = conn.set("test_message", "Hello world")
                get_record = conn.get("test_message")
                delete_record = conn.delete("test_message")
                get_deleted_record = conn.get("test_messsage")
                if set_record == True and get_record == "Hello world" and delete_record == 1 and get_deleted_record == None:
                    redis_db_status = "active"
                else:
                    redis_db_status = "inactive"
                #except Exception as ex1:
                #    redis_exception_msg = ""
                #    redis_exception_msg += ex1
            except Exception as ex:
                redis_exception_msg += ex1
                #exit('Failed to connect, terminating.')
            finally:
                return( redis_db_status, redis_exception_msg)

#function for testing elasticsearch db connection
    def es_db(self):
        es_exception_msg = ""
        try:
          es = Elasticsearch(
              http_auth=('elastic', 'changeme'),
              port=9200,
          )
          res = es.info();
          #print res['cluster_name']
          #print "Connected"
          node_info = es.nodes.info()
          if (node_info['_nodes']['successful'] >=1):
            es_status= "active"
          else:
            es_status = "inactive, no nodes running or available"


        except Exception as ex:
            # es_exception_msg = ""
            es_exception_msg += "Error: "

        finally:
            return(es_status, es_exception_msg)

#function to test for mysql db

    def mysql_db(self):


        try:
            mysql_exception_msg, mysql_db_status = ""
            cnx = mysql.connector.connect(user='root',
            password='',
            host='localhost',
            database='test')
            cursor = cnx.cursor()
            cursor.execute("SELECT VERSION()")
            results = cursor.fetchone()
            # Check if anything at all is returned
            if results:
                mysql_db_status= " Database is Active"
            else:
                mysql_db_status = "There may be a problem with the database"
            # print('Connected')
            cnx.close()
        except Exception as ex:
            # if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            #     print("Something is wrong with your user name or password")
            # elif err.errno == errorcode.ER_BAD_DB_ERROR:
            #     print("Database does not exist")
            # else:
            #     print(err)

            mysql_exception_msg = "Error: " + err

        finally:
            return(mysql_db_status, mysql_exception_msg)
